#!/usr/bin/env bash

IMG=exfat.img
MKFS=mkfs.exfat
FSCK=fsck.exfat
MNT=mount
MNT_POINT=/mnt/exfattest
PASS_COUNT=0
TEST_COUNT=4

cleanup() {
        umount $MNT_POINT
        rm -rf $IMG
        echo ""
        echo "Passed ${PASS_COUNT} of ${TEST_COUNT}"
        if [ ${PASS_COUNT} -ne ${TEST_COUNT} ]; then
                exit 1
        else
                exit 0
        fi
}


#Create img:
dd if=/dev/zero of=$IMG bs=1M count=200
if [ $? -ne 0 ]; then
        echo "ERR: Make img failed!"
        exit 1
else
        echo "--- Created img file: $IMG ---"
fi

#mkfs test
echo ""
$MKFS $IMG --verbose
if [ $? -ne 0 ]; then
        echo "ERR: mkfs.exfat failed!"
        exit 1
else
        echo "--- mkfs test done ---"
        PASS_COUNT=$((PASS_COUNT + 1))
fi

#fsck test
echo ""
$FSCK $IMG --verbose
if [ $? -ne 0 ]; then
        echo "ERR: fsck.exfat failed!"
        exit 1
else
        echo "--- fsck test done ---"
        PASS_COUNT=$((PASS_COUNT + 1))
fi

#mount test
echo ""
mkdir -p $MNT_POINT
$MNT -o loop $IMG $MNT_POINT
$MNT | grep $MNT_POINT | grep exfat
if [ $? -ne 0 ]; then
        echo "ERR: mount failed!"
        exit 1
else
        echo "--- mount test done ---"
        PASS_COUNT=$((PASS_COUNT + 1))
fi

#RW test
mkdir -p $MNT_POINT/testdir
echo "This is a test" > $MNT_POINT/testdir/testfile
cat $MNT_POINT/testdir/testfile | grep "This is a test"
if [ $? -ne 0 ]; then
        echo "ERR: Read or write failed!"
        exit 1
else
        echo "--- RW test done ---"
        PASS_COUNT=$((PASS_COUNT + 1))
fi

cleanup